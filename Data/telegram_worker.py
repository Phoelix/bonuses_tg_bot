from telegram import (ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup)

class Telegram_Worker:

    def __init__(self, bot, update):
        self.bot = bot
        self.update = update
        self.user = update.effective_user
        self.message = update.effective_message

    def send_msg(self, message:str, kboard:dict=None,
                 user:int=None, parse_mode='HTML', reply=False):
        if not user: user = self.user.id
        kboard = self.__proc_kboard(kboard, reply)
        return self.bot.send_message(
                text=message,
                chat_id=user,
                reply_markup=kboard,
                parse_mode=parse_mode)

    def inline(self, message:str, kboard:dict=None,
                 parse_mode='HTML', reply=False):
        kboard = self.__proc_kboard(kboard, reply)
        return self.bot.edit_message_text(
                text=message,
                chat_id=self.update.callback_query.message.chat_id,
                message_id=self.update.callback_query.message.message_id,
                reply_markup=kboard,
                parse_mode=parse_mode)

    def allert(self, message, query_id=None, url=None, allert=False):
        if not query_id:
                query_id = self.update.callback_query.id
        return self.bot.answer_callback_query(
                text=message,
                callback_query_id=query_id,
                url=url,
                show_alert=allert)

    def __proc_kboard(self, kboard:dict, reply=False):
        if reply:
            kboard = ReplyKeyboardMarkup(kboard, resize_keyboard=True)
        else:
            temp_board=[]
            for key in kboard:
                if kboard[key] is dict:
                    row_in_board = []
                    for subkey in kboard[key]:
                        row_in_board.append(InlineKeyboardButton(text=subkey,
                                                                 callback_data=key[subkey]))
                    temp_board.append(row_in_board)
                else:
                    temp_board.append([InlineKeyboardButton(text=key,
                                                            callback_data=kboard[key])])
            kboard = InlineKeyboardMarkup(temp_board)

        return kboard