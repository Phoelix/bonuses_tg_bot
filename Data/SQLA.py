#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, MetaData
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relation
from sqlamp import DeclarativeMeta


engine = create_engine('sqlite:///engine.db', echo=True)
metadata = MetaData(engine)
Node = declarative_base(metadata=metadata, metaclass=DeclarativeMeta)
Session = sessionmaker(bind=engine, autoflush=True)


class User(Node):
    __tablename__ = 'users'
    __mp_manager__ = 'mp'
    id = Column(Integer, primary_key=True)
    tgid = Column(Integer)
    username = Column(String(35))
    fullname = Column(String(255))
    status = Column(String(15), default="client")
    lang = Column(String(5))
    bonuses = Column(Integer, default=0)
    UAH = Column(Integer, default=0)
    refer_id = Column(ForeignKey('users.tgid'))

    referal = relation("User", remote_side=[tgid])


class Variable(Node):
    __tablename__ = 'variables'
    name = Column(String(15), primary_key=True)
    value = Column(String(255))


class Message(Node):
    __tablename__ = 'messages'
    name = Column(String(32), primary_key=True)
    ua = Column(String)
    en = Column(String)


class Menu(Node):
    __tablename__='menu'
    id = Column(Integer, primary_key=True)
    name = Column(String(511), primary_key=True)
    picture = Column(String)
    price = Column(Integer)
    units = Column(String)

