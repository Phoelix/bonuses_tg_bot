from sqlalchemy \
    import or_
import sqlalchemy.orm.exc \
    as db_error

from Data.SQLA \
    import Session, User, Variable, Message, Menu
import Data.bot_str_names as bsn
import Data.UA as UA


class SQLWorker():
    session = Session()

    def factory_settings(self):
        start_bonuses = Variable(name=bsn.start_bonuses, value="1")
        start_uah = Variable(name=bsn.UAH_bonuses, value="0")
        def_menu_item = Menu(id=0, name='coffee', price=6, units= bsn.bonuses)
        def_product = Variable(bsn.def_product, 0)
        self.session.merge(start_bonuses)
        self.session.merge(start_uah)
        self.session.merge(def_menu_item)
        self.session.merge(def_product)



    def close(self):
        self.session.commit()
        self.session.close()

    def create_user(self, from_user, refer_id=None, bonuses=0, UAH=0, lang='ua'):
        try:
            client = self.get_user(from_user.id)
        except db_error.NoResultFound:
            client = User(
                tgid=from_user.id,
                username=from_user.username,
                fullname=from_user.full_name,
                status=bsn.client,
                lang=lang,
                bonuses=bonuses,
                UAH=UAH)
            if refer_id: client.refer_id=refer_id

            self.session.add(client)

        return client

    def get_user(self, id=None):
        return self.session.query(User)\
                        .filter(or_(User.tgid==id,User.id==id))\
                            .first()

    def update_user(self, tgid, from_user=None, **kwargs):
        """**kwargs: status, lang, bonuses, UAH"""
        client = self.get_user(tgid)
        for key in kwargs:
            if hasattr(User, key):
                setattr(client, key, kwargs[key])
        if from_user:
            client.username = from_user.username
            client.fullname = from_user.full_name
        self.session.add(client)

    def get_variable(self, *names):
        if bsn.start_bonuses in names:
            try:
                data = self.session.query(Variable) \
                    .filter(Variable.name.in_(names)) \
                    .all()
            except db_error.NoResultFound:
                self.factory_settings()
        data = self.session.query(Variable) \
            .filter(Variable.name.in_(names)) \
            .all()
        return data

    def update_or_set_variable(self, name, data:str):
        variable = Variable(name, data)
        self.session.merge(variable)

    def get_msg(self, *names):
        return self.session.query(Message)\
                        .filter(Message.name.in_(names))\
                            .all()

    def set_msg(self, name, text, lang):
        msg = Message(name)
        setattr(msg, lang, text)

    def get_menu_items(self, names=None):
        if names:
            data = self.session.query(Menu)\
                        .filter(Menu.name.in_(names))\
                            .all()
        else:
            data = self.session.query(Menu).all()
        return data

    def add_menu_item(self, name, price:int, units:str, picture:str=None):
        item = Menu(name, picture=picture, price=price, units=units)
        try:self.session.add(item)
        except db_error.MultipleResultsFound:
            return "exists"

    def update_menu_item(self, loaded_item):
        self.session.merge(loaded_item)

    def factory_texts(self):
        wellcome = Message(bsn.wellcome, UA.welcome)