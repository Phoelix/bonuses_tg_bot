import logging

logging.basicConfig(format="%(asctime)s  %(levelname)s -> %(message)s",
                    level=logging.INFO,
                    handlers=[
                        logging.FileHandler("Work.log"),
                        logging.StreamHandler()
                    ])

log = logging.getLogger(__name__)


def user_log(from_user):  # User :id :name template for logs
    template = str(from_user.id)
    if from_user.username: template += f'::{from_user.username}'
    else: template += f'::{from_user.first_name}'
    return template


def strikethrough_text(string, length=1, symbol='̶'):
    return symbol.join(string[i:i+length] for i in range(0,len(string),length))









